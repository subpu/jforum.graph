package com.apobates.forum.graph.editor;

import javax.servlet.http.HttpServletRequest;

/**
 * 上传的参数接口
 *
 * @author xiaofanku
 * @since 20200528
 */
public interface UploadInitParamers extends CommonInitParamers{
    /**
     * 上传的输入项名字
     *
     * @return
     */
    String getUploadFileInputName();
    
    /**
     * http servlet request
     *
     * @return
     */
    HttpServletRequest getRequest();
}