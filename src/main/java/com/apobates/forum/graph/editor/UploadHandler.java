package com.apobates.forum.graph.editor;

import java.util.Optional;

/**
 * 整合的句柄接口
 *
 * @author xiaofanku
 * @since 20200528
 */
public interface UploadHandler extends HandleOnUploadComplete,HandleOnUploadException,HandleOnUploadBeforeReturn{
    /**
     * 保存的文件名
     *
     * @param fileExtensionName 文件的扩展名,全小写.包含点字符
     * @return
     */
    Optional<String> generateFileName(String fileExtensionName);
    
    /**
     * 允许使用的文件扩展名,必须都小写
     *
     * @return
     */
    String[] allowFileExtension();
    
    /**
     * 上传允许的最大字节数
     *
     * @return
     */
    long allowMaxBytes();
}