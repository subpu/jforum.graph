package com.apobates.forum.graph.editor;

import java.util.Optional;

/**
 * 公共的上传初始化参数接口
 * @author xiaofanku
 * @since 20200528
 */
public interface CommonInitParamers {
    /**
     * 文件的保存目录
     * @return 
     */
    String getFileSaveDir();
    
    /**
     * 回调函数的查询字符串
     * @return 
     */
    Optional<String> getCallbackFunctionName();
}