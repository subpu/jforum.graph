package com.apobates.forum.graph.editor;

import java.io.File;
import java.util.Optional;

/**
 * 上传结束后,结果返回前的处理句柄
 * @author xiaofanku
 * @since 20200528
 */
@FunctionalInterface
public interface HandleOnUploadBeforeReturn {
    /**
     * 
     * @param uploadDir
     * @param fileName
     * @param callBackFunction 
     */
    void otherProcess(File uploadDir, String fileName, Optional<String> callBackFunction);
}