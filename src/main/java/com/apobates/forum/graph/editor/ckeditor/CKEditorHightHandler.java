package com.apobates.forum.graph.editor.ckeditor;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import com.apobates.forum.graph.editor.AbstractHandler;
import com.google.gson.Gson;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * CKeditor V4.11.4上传的回调句柄
 *
 * @author xiaofanku
 * @since 20200528
 */
public class CKEditorHightHandler extends AbstractHandler {
    private final long allowMaxFileBytes;

    /**
     *
     * @param frontVisitPath
     * @param allowMaxFileBytes 限制文件上传的大小,单位字节
     */
    public CKEditorHightHandler(String frontVisitPath, long allowMaxFileBytes) {
        super(frontVisitPath);
        this.allowMaxFileBytes = allowMaxFileBytes;
    }

    /**
     * 不限制文件上传的大小,表示代码不作上传前的检查.交由容器
     *
     * @param frontVisitPath
     */
    public CKEditorHightHandler(String frontVisitPath) {
        super(frontVisitPath);
        this.allowMaxFileBytes = -1;
    }

    @Override
    public String getResponse(String fileName, Optional<String> callFunction) {
        return getFrontVisitPath() + fileName;
    }

    @Override
    public Optional<String> generateFileName(String fileExtensionName) {
        String tmp = String.format("snapshot-%s", getDateFullFormatString().concat(RandomStringUtils.randomNumeric(4)))+fileExtensionName;
        return Optional.of(tmp);
    }

    @Override
    public long allowMaxBytes() {
        return allowMaxFileBytes;
    }

    /**
     * CK4:V4.11.4的响应格式
     * {@link ://ckeditor.com/docs/ckeditor4/latest/guide/dev_file_upload.html}
     *
     * @param isCompleted 是否完成上传
     * @param message 消息内容
     * @param fileName 保存的文件
     * @param imageUrl 上传成功后的访问地址
     * @return
     */
    public static String buildCkeditorResponse(boolean isCompleted, String message, String fileName, String imageUrl) {
        Map<String, Object> data = new HashMap<>();
        data.put("uploaded", isCompleted ? "1" : "0");
        if (null != fileName) {
            data.put("fileName", fileName);
        }
        if (null != imageUrl) {
            data.put("url", imageUrl);
        }
        if (!isCompleted) {
            data.put("error", Map.ofEntries(Map.entry("message", StringUtils.defaultString(message, "未知的网络错误"))));
        }
        return new Gson().toJson(data);
    }

    /**
     * CK4:V4.11.4的失败的响应格式
     * {@link ://ckeditor.com/docs/ckeditor4/latest/guide/dev_file_upload.html}
     *
     * @param isCompleted 是否完成上传
     * @param message 错误消息内容
     * @return
     */
    public static String buildCkeditorResponse(boolean isCompleted, String message) {
        return buildCkeditorResponse(isCompleted, message, null, null);
    }

    /**
     * 格式化当前日期为yyyyMMddHHmmssSSS<19>
     *
     * @return
     */
    public static String getDateFullFormatString() {
        return LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS"));
    }
}
