package com.apobates.forum.graph.editor;

import java.util.Optional;

/**
 * 上传结束后的处理句柄
 * 
 * @author xiaofanku
 * @since 20200528
 */
@FunctionalInterface
public interface HandleOnUploadComplete {
    /**
     * 
     * @param fileName
     * @param callFunction
     * @return 
     */
    String getResponse(String fileName, Optional<String> callFunction);
}