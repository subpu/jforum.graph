package com.apobates.forum.graph.editor;

import java.io.IOException;

/**
 * 上传过程中出现IO异常时的处理句柄
 * 
 * @author xiaofanku
 * @since 20200528
 */
@FunctionalInterface
public interface HandleOnUploadException {
    /**
     * 
     * @param e
     * @throws IOException 
     */
    void forceException(IOException e) throws IOException;
}